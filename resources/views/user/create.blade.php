@extends("layouts/layout")
@section('navbar')
    @include('layouts.partials.navbar')
@endsection


@section('content')
    <div class="container">
        <h1>Add User</h1>
        <hr>
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif
        <form class="form-horizontal" action="{{route('admin.store')}}" method="post" enctype="multipart/form-data">
            <div class="row">
                <!-- left column -->
                <div class="col-md-3">
                    <div class="text-center">
                        <img id="avatar" src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                        <h6>Upload a different photo...</h6>
                        <input type="file" class="form-control" name="imageName">
                        @if($errors && $errors->has('imageName'))
                            @foreach($errors->get('imageName') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- edit form column -->
                <div class="col-md-9 personal-info">
                    <h3>Personal info</h3>
                    @csrf
                    <div class="form-group">
                        <label class="col-lg-3 control-label">First name:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="name" value="User Name">
                        </div>
                        @if($errors && $errors->has('name'))
                            @foreach($errors->get('name') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Email:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="email" value="User Email">
                        </div>
                        @if($errors && $errors->has('email'))
                            @foreach($errors->get('email') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password:</label>
                        <div class="col-md-8">
                            <input id="password" type="password"
                                   class="form-control @error('password') is-invalid @enderror" name="password" required
                                   autocomplete="new-password">
                        </div>
                        @if($errors && $errors->has('password'))
                            @foreach($errors->get('password') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password-confirm" class="col-md-3 control-label">Confirm Password:</label>
                        <div class="col-md-8">
                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required autocomplete="new-password">
                        </div>
                        @if($errors && $errors->has('password_confirmation'))
                            @foreach($errors->get('password_confirmation') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary" value="Save Changes">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <hr>
@endsection
