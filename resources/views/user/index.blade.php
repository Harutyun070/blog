@extends('layouts.layout')

@section('navbar')
    @include('layouts.partials.navbar')
@endsection
@section('content')
    <div class="container">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif
        @foreach ($users->items() as $user)
            <section class="container mt-4 mb-4">
                <div class="container">
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <div class="d-flex flex-row border rounded">
                                <div class="p-0 w-25">
                                    @if($user->image)
                                        <img src="{{asset('/storage/uploads/users/'.$user->id.'/'.$user->image)}}"
                                             class="img-thumbnail border-0"/>
                                    @else
                                        <img src="{{asset('/uploads/users/default/downd.png')}}"
                                             class="img-thumbnail border-0"/>
                                    @endif
                                </div>
                                <div class="pl-3 pt-2 pr-2 pb-2 w-75 border-left">
                                    <h4 class="text-primary">{{$user->name}}</h4>
                                    <h5 class="text-info">{{$user->email}}</h5>
                                    <ul class="m-0 float-left" style="list-style: none; margin:0; padding: 0">
                                        <li><i class="fab fa-facebook-square"></i> Facebook</li>
                                        <li><i class="fab fa-twitter-square"></i>{{$user->created_at}}</li>
                                    </ul>
                                    <p class="text-right m-0"><a href="{{route('admin.edit', $user->id)}}"
                                                                 class="btn btn-primary"><i class="far fa-user"></i>
                                            View Profile</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endforeach
    </div>
    {{ $users->links() }}
@endsection
