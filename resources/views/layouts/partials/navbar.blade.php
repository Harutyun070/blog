<nav class="navbar navbar-expand-md navbar-dark bg-dark">


    <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav">
            <a href="{{route('user.products')}}" class="nav-item nav-link ">My Cars</a>
            @can('isUser')
            <a href="{{route('user.show')}}" class="nav-item nav-link">Profile</a>
            @endcan
            <a href="{{route('product.index')}}" class="nav-item nav-link">Cars</a>
            <a href="{{route("product.create")}}" class="nav-item nav-link">Add Car</a>
            @can('isAdmin')
                <a href="{{route("admin.users")}}" class="nav-item nav-link">All Users</a>
                <a href="{{route("admin.create")}}" class="nav-item nav-link">Add User</a>
            @elsecan('isManager')
                <a href="{{route("admin.users")}}" class="nav-item nav-link">All Users</a>
            @endcan
        </div>
        <div class="navbar-nav ml-auto">
            @can('isAdmin')
                    <a href="{{route('profile.show')}}" class="nav-item nav-link">Profile</a>
             @elsecan('isManager')
                <a href="{{route('user.show')}}" class="nav-item nav-link">Profile</a>
                @endcan
            <a href="{{route('logout')}}" class="nav-item nav-link">Log Out</a>
        </div>
    </div>
</nav>
