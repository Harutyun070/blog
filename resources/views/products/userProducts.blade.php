@extends('layouts/layout')

@section('navbar')
    @include('layouts.partials.navbar')
@endsection

@section('content')
    <div class="container">
        <div class="row" id="ads">
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            @foreach($products as $product)
            <!-- Category Card -->
                <div class="col-md-4 m-5">
                    <div class="card rounded">
                        <div class="card-image">
                            <span class="card-notify-badge">{{$product->category}}</span>
                            <span class="card-notify-year">{{$product->year}}</span>
                            @if($product->image_name)
                                <img class="img-fluid"
                                     src="{{asset('/storage/uploads/products/'.$product->id.'/'.$product->image_name)}}"
                                     alt="Alternate Text"/>
                            @else
                                <img class="img-fluid"
                                     src="{{asset('/uploads/products/default/car-default.jpg')}}"
                                     alt="Alternate Text"/>
                            @endif
                        </div>
                        <div class="card-image-overlay m-auto">
                            <span class="card-detail-badge">Used</span>
                            <span class="card-detail-badge">${{$product->price}}</span>
                            <span class="card-detail-badge">{{$product->odometr}} Kms</span>
                            <span class="card-detail-badge">owner: {{$product->user->name}}</span>
                        </div>
                        <div class="card-body text-center">
                            <div class="ad-title m-auto">
                                <h5>{{$product->name}}</h5>
                            </div>
                            <a class="ad-btn" href="{{route('product.show', ['product' => $product->id])}}">View</a>
                        </div>
                    </div>
                </div>
            @endforeach
            <div>
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style>

        @import url('https://fonts.googleapis.com/css?family=Open+Sans');
        @import url('https://fonts.googleapis.com/css?family=Montserrat');

        body {
            font-family: 'Montserrat', sans-serif;

        }

        /* Category Ads */

        #ads {
            margin: 30px 0 30px 0;

        }

        #ads .card-notify-badge {
            position: absolute;
            left: -10px;
            top: -20px;
            background: #f2d900;
            text-align: center;
            border-radius: 30px 30px 30px 30px;
            color: #000;
            padding: 5px 10px;
            font-size: 14px;

        }

        #ads .card-notify-year {
            position: absolute;
            right: -10px;
            top: -20px;
            background: #ff4444;
            border-radius: 50%;
            text-align: center;
            color: #fff;
            font-size: 14px;
            width: 50px;
            height: 50px;
            padding: 15px 0 0 0;
        }

        #ads .card-detail-badge {
            background: #f2d900;
            text-align: center;
            border-radius: 30px 30px 30px 30px;
            color: #000;
            padding: 5px 10px;
            font-size: 14px;
        }

        #ads .card:hover {
            background: #fff;
            box-shadow: 12px 15px 20px 0px rgba(46, 61, 73, 0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }

        #ads .card-image-overlay {
            font-size: 20px;

        }

        #ads .card-image-overlay span {
            display: inline-block;
        }

        #ads .ad-btn {
            text-transform: uppercase;
            width: 150px;
            height: 40px;
            border-radius: 80px;
            font-size: 16px;
            line-height: 35px;
            text-align: center;
            border: 3px solid #e6de08;
            display: block;
            text-decoration: none;
            margin: 20px auto 1px auto;
            color: #000;
            overflow: hidden;
            position: relative;
            background-color: #e6de08;
        }

        #ads .ad-btn:hover {
            background-color: #e6de08;
            color: #1e1717;
            border: 2px solid #e6de08;
            background: transparent;
            transition: all 0.3s ease;
            box-shadow: 12px 15px 20px 0px rgba(46, 61, 73, 0.15);
        }

        #ads .ad-title h5 {
            text-transform: uppercase;
            font-size: 18px;
        }
    </style>
@endsection
