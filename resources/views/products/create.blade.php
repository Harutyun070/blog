@extends('layouts.layout')

@section('navbar')
    @include('layouts.partials.navbar')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12" style="width: 50%;">
                <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp" placeholder="Name">
                        @if($errors && $errors->has('name'))
                            @foreach($errors->get('name') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Price</label>
                        <input type="number" name="price" class="form-control" id="exampleInputPassword1"
                               placeholder="Price">
                        @if($errors && $errors->has('price'))
                            @foreach($errors->get('price') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <input type="text" name="description" class="form-control" id="exampleInputPassword1"
                               placeholder="Description">
                        @if($errors && $errors->has('description'))
                            @foreach($errors->get('description') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Category</label>
                        <input type="text" class="form-control" name="category" id="exampleInputPassword1"
                               placeholder="Category">
                        @if($errors && $errors->has('category'))
                            @foreach($errors->get('category') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Odometr</label>
                        <input type="number" class="form-control" name="odometr" id="exampleInputPassword1"
                               placeholder="Odometr">
                        @if($errors && $errors->has('odometr'))
                            @foreach($errors->get('odometr') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Year</label>
                        <input type="number" class="form-control" name="year" id="exampleInputPassword1"
                               placeholder="Year">
                        @if($errors && $errors->has('year'))
                            @foreach($errors->get('year') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Image</label>
                        <input type="file" class="form-control" name="image" id="exampleInputPassword1"
                               placeholder="Your Car Image">
                        @if($errors && $errors->has('image'))
                            @foreach($errors->get('image') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
