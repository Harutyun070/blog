@extends("layouts/layout")
@section('navbar')
    @include('layouts.partials.navbar')
@endsection

@section('content')
    <div class="container">
        @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
        @endif
        <div class="row">
            <div class="col-12" style="width: 50%;">

                <form action="{{route('product.update', ['product' => $product->id])}}" method="POST"
                      enctype="multipart/form-data">
                    @method('PUT')
                    @csrf

                    <input type="hidden" name="id" value="{{$product->id}}">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1"
                               aria-describedby="emailHelp" value="{{$product->name}}">
                        @if($errors && $errors->has('name'))
                            @foreach($errors->get('name') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Price</label>
                        <input type="number" name="price" class="form-control" id="exampleInputPassword1"
                               value="{{$product->price}}">
                        @if($errors && $errors->has('price'))
                            @foreach($errors->get('price') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description</label>
                        <input type="text" name="description" class="form-control" id="exampleInputPassword1"
                               value="{{$product->description}}">
                        @if($errors && $errors->has('description'))
                            @foreach($errors->get('description') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Category</label>
                        <input type="text" class="form-control" name="category" id="exampleInputPassword1"
                               value="{{$product->category}}">
                        @if($errors && $errors->has('category'))
                            @foreach($errors->get('category') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Odometr</label>
                        <input type="number" class="form-control" name="odometr" id="exampleInputPassword1"
                               value="{{$product->odometr}}">
                        @if($errors && $errors->has('odometr'))
                            @foreach($errors->get('odometr') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Year</label>
                        <input type="number" class="form-control" name="year" id="exampleInputPassword1"
                               value="{{$product->year}}">
                        @if($errors && $errors->has('year'))
                            @foreach($errors->get('year') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Image</label>
                        <input type="file" class="form-control" name="image" id="exampleInputPassword1"
                               placeholder="Your Car Image">
                        @if($errors && $errors->has('image'))
                            @foreach($errors->get('image') as $value)
                                <div class="alert alert-danger" role="alert">
                                    {{$value}}
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
                <form action="{{route('product.destroy', ['product' => $product->id])}}" method="post">
                    <button class="add-to-cart btn btn-default" type="submit">Delete</button>
                    @method('DELETE')
                    @csrf
                </form>

            </div>
        </div>
    </div>
@endsection
