import Vue from "vue";

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import App from './components/app'
import router from './router'
import components from './components'
import * as Echo from "pusher-js";
import Pusher from "pusher-js/src/core/pusher";
import store from './store'
import StarRating from 'vue-star-rating'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading, {
    canCancel: true,
    color: '#000000',
    loader: 'spinner',
    width: 64,
    height: 64,
    backgroundColor: '#ffffff',
    opacity: 0.5,
    zIndex: 999,
})






/**
 * register 'vue-infinite-scroll' globally
  */

window.infiniteScroll =  require('vue-infinite-scroll');
window.store = store;


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


store.dispatch('authUser').then(() => {
    const app = new Vue({
        el: '#app',
        directives: {infiniteScroll},
        store,
        router,
        render: h => h(App)
    });
});

