export default {
    state: {
        user: [],
    },
    mutations: {
        loginUser(state, data) {
            state.user = data
        },

        logoutUser(state) {
            state.user = []
        }

    },
    actions: {
        authUser(ctx) {
            return new Promise(function (resolve, reject) {
                let token = cookie.get('access_token')
                if (!token) {
                    resolve()
                } else {
                    axios.get('/api/auth')
                        .then(response => {
                            if (response.data.success) {
                                ctx.commit('loginUser', response.data.authUser)
                            }
                            resolve()
                        })
                        .catch(() => {
                            reject()
                        })
                }
            })
        },
        byLogin(ctx, data) {
            return new Promise(resolve, reject => {
                axios.post('/api/login', {
                    email: this.email,
                    password: this.password
                })
                    .then( response => {
                        if(response.data.success){
                            cookie.set('access_token', response.data.access_token);
                            axios.defaults.headers.common['Authorization'] = 'Bearer '+ cookie.get('access_token');
                            ctx.commit('loginUser', response.data.user)
                            this.$router.push({name: 'userProducts'})
                        }
                        else {
                            this.error = "Something go wrong!"
                        }
                    })
                    .catch(error => {
                        this.error = error.response.data.message
                        this.errors = error.response.data.errors;
                    });
            } )
            ctx.commit('loginUser', data.user)
        },

        logoutUser(ctx) {
            ctx.commit('logoutUser', [])
        }

    },

    getters: {
        authUser(state) {
            return state.user
        }
    }
}
