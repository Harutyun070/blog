import Vue from 'vue'
import VueRouter from 'vue-router'
import auth from './middleware/auth'
import roleUser from './middleware/roleUser'


Vue.use(VueRouter);


const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../components/pages/product/products'),
    },

    {
        path: '/products',
        name: 'products',
        meta: {
          title: 'Cars'
        },
        component: () => import('../components/pages/product/products'),
    },

    {
        path: '/users',
        name: 'users',
        meta: {
            title: 'All Users',
            middleware: [auth,roleUser]
        },
        component: () => import('../components/pages/user/users')
    },
    {
        path: '/userProducts',
        name: 'userProducts',
        meta: {
            title: 'Your Cars',
            middleware: auth
        },
        component: () => import('../components/pages/product/userProducts')
    },
    {
        path: '/product/:id',
        name: 'productPage',
        component: () => import('../components/pages/product/productPage')
    },
    {
        path: '/createProduct',
        name: 'createProduct',
        meta: {
            title: 'Add Car',
            middleware: auth
        },
        component: () => import('../components/pages/product/createProduct')
    },
    {
        path: '/login',
        name: 'login',
        meta: {
          title: 'Log In'
        },
        component: () => import('../components/pages/auth/login')
    },
    {
        path: '/register',
        name: 'register',
        meta: {
          title: 'Sign Up'
        },
        component: () => import('../components/pages/auth/register')
    },
    {
        path: '/product/:id/edit',
        name: 'edit',
        meta: {
            title: 'Car Edit',
            middleware: auth
        },
        component: () => import('../components/pages/product/edit')
    },

    {
        path: '/user/:id/edit',
        name: 'editUser',
        meta: {
            title: 'User Edit',
            middleware: [auth,roleUser]
        },
        component: () => import('../components/pages/user/editUser')
    },
    {
        path: '/upload/xls',
        name: 'addXls',
        meta: {
            title: 'Add Users or Products From Xls',
            middleware: [auth,roleUser]
        },
        component: () => import('../components/pages/product/addXls')
    }
];

const router = new VueRouter({
    routes,
    mode: 'history',
});

function nextFactory(context, middleware, index) {
    const subsequentMiddleware = middleware[index];
    // If no subsequent Middleware exists,
    // the default `next()` callback is returned.
    if (!subsequentMiddleware) return context.next;

    return (...parameters) => {
        // Run the default Vue Router `next()` callback first.+    context.next(...parameters);
        // Then run the subsequent Middleware with a new+    // `nextMiddleware()` callback.
        const nextMiddleware = nextFactory(context, middleware, index + 1);
        subsequentMiddleware({...context, next: nextMiddleware});
    };
}

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware)
            ? to.meta.middleware
            : [to.meta.middleware];

        const context = {
            from,
            next,
            router,
            to,
        };
        const nextMiddleware = nextFactory(context, middleware, 1);
        return middleware[0]({...context, next: nextMiddleware});
    }
    return next();
});

router.beforeEach((toRoute, fromRoute, next) => {
    window.document.title = toRoute.meta && toRoute.meta.title ? toRoute.meta.title : 'SHOP';
    next();
})

export default router

