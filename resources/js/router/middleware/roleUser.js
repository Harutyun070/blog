export default function auth({ next, router }) {
    let authUser = store.getters.authUser;
    if(authUser && Object.keys(authUser).length && authUser.roles[0].name !== 'admin'){
        return router.push({ name: 'home' });
    }
    return next();
}
