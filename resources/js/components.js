import Vue from 'vue'

Vue.component('navbar', () => {return import('./components/layouts/navbar')});
Vue.component('nav-foot', ()=> { return import('./components/layouts/navFoot')});
Vue.component('star-rating',()=> {return import("vue-star-rating")});
Vue.component('my-map', ()=> { return import('./components/pages/map/map')});
