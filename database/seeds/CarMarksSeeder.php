<?php

use Illuminate\Database\Seeder;

class CarMarksSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        $makes = [];

        foreach ($this->makes() as $make)
        {
            $make['mark_id'] = $make['id'];
            $make = \Illuminate\Support\Arr::except($make, 'id');
            $makes[] = array_merge($make, [
                'created_at' => $time,
                'updated_at' => $time
            ]);
        }
        \Illuminate\Support\Facades\DB::table('car_marks')->insert($makes);

    }

    /**
     * Get data to be inserted into the vehicle_makes table.
     *
     * @return array
     */
    public function makes()
    {
        return json_decode(file_get_contents(__DIR__.'/Data/makes.json'), true);
    }
}
