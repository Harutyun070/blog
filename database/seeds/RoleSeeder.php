<?php

use App\Models\Role;
use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            Role::truncate();
            $roleNames = ['admin', 'moderator', 'user'];
            $roles = [];
            foreach ($roleNames as $name) {
                $roles[] = [
                    'name' => $name
                ];
            }
            Role::insert($roles);

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
        }
    }
}
