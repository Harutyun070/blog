<?php

use Illuminate\Database\Seeder;

class CarModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $time = date('Y-m-d H:i:s');
        $models = [];

        foreach ($this->models() as $model)
        {
            $model = \Illuminate\Support\Arr::except($model, 'id');
            $models[] = array_merge($model, [
                'created_at' => $time,
                'updated_at' => $time
            ]);
        }
        \Illuminate\Support\Facades\DB::table('car_models')->insert($models);
    }

    /**
     * Get data to be inserted into the vehicle_models table.
     *
     * @return mixed
     */
    public function models()
    {
        return json_decode(file_get_contents(__DIR__.'/Data/models.json'), true);
    }
}
