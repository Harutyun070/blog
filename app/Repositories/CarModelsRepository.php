<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 6/5/20
 * Time: 3:23 PM
 */

namespace App\Repositories;


use App\CarModel;
use App\Contracts\CarModelsInterface;

/**
 * Class CarModelsRepository
 * @package App\Repositories
 */
class CarModelsRepository implements CarModelsInterface
{
    /**
     * @var CarModel
     */
    protected $model;

    /**
     * CarModelsRepository constructor.
     * @param CarModel $model
     */
    public function __construct(CarModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param $markId
     * @return mixed
     */
    public function getModels($markId)
    {
        return $this->model->where('mark_id', $markId)->select('id', 'name')->get();
    }
}
