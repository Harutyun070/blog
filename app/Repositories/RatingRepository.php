<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 6/1/20
 * Time: 2:48 PM
 */

namespace App\Repositories;


use App\Contracts\RatingInterface;
use App\Models\Rating;

/**
 * Class RatingRepository
 * @package App\Repositories
 */
class RatingRepository implements RatingInterface
{
    /**
     * @var Rating
     */
    protected $model;

    /**
     * RatingRepository constructor.
     * @param Rating $model
     */
    public function __construct(Rating $model)
    {
        $this->model = $model;
    }

    public function store($userId ,$data)
    {
       return $this->model->updateOrCreate(['user_id' => $userId], $data);
    }
}
