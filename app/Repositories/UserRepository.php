<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 4/27/20
 * Time: 10:08 PM
 */

namespace App\Repositories;


use App\Contracts\UserInterface;
use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository implements UserInterface
{
    /**
     * @var User
     */
    protected $model;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $skip
     * @param $take
     * @return mixed
     */
    public function getAll($skip, $take)
    {
        return $this->model->skip($skip)->take($take)->get();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function index($userId)
    {
       return $this->model->where('id', $userId)->with('roles')->first();
    }

    /**
     * @param $id
     * @param $userData
     * @return mixed
     */
    public function update($id, $userData)
    {
        return $this->model->where('id', $id)->update($userData);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        return $this->model->where('id', $id)->delete();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function show($userId)
    {
        return $this->model->where('id', $userId)->with('address')->select('id', 'name', 'image')->first();
    }

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function updateOrCreateAddress($userId, $data)
    {
        return $this->model->where('id', $userId)->first()
            ->address()->updateOrCreate(['addressable_id' => $userId, 'addressable_type' => User::class], $data);
    }

}
