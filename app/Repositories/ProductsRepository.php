<?php

namespace App\Repositories;


use App\Contracts\ProductsInterface;
use App\Models\Product;

/**
 * Class ProductsRepository
 * @package App\Http\Repositories
 */
class ProductsRepository implements ProductsInterface
{
    /**
     * @var Product
     */
    protected $model;

    /**
     * ProductsRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByIdWithComments($id)
    {
        return $this->model->where('id', $id)->with(['comment.user', 'ratings'])->first();
    }

    /**
     * @param $skip
     * @param $take
     * @return mixed
     */
    public function getAll($skip, $take)
    {
        return $this->model
            ->with('carModel')
            ->skip($skip)->take($take)->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        return $this->model->where('id', $id)->update($data);
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function destroy($productId)
    {
        return $this->model->where('id', $productId)->delete();
    }

    /**
     * @param $userId
     * @param $skip
     * @param $take
     * @return mixed
     */
    public function getUserProducts($userId, $skip, $take)
    {
        return $this->model->skip($skip)->take($take)->where('user_id', $userId)->get();
    }

    /**
     * get all searching products.
     * @param $data
     * @return
     */
    public function getSearchingProducts($data)
    {
        return $this->model->where('name', 'like', '%' .$data. '%')->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getByParams($data)
    {
        return $this->model
            ->join('car_models', 'car_models.id', 'products.model_id')
            ->join('car_marks', 'car_marks.mark_id', 'car_models.mark_id')
            ->when(isset($data['mark_id']), function ($qb) use ($data){
              return $qb->where('car_marks.mark_id', $data['mark_id']);
            })
            ->when(isset($data['model_id']), function ($qb) use ($data){
              return $qb->where('car_models.id', $data['model_id']);
            })
            ->when(isset($data['minPrice']),function ($qb) use ($data){
                return $qb->where('price', '>=', $data['minPrice']);
            })
            ->when(isset($data['maxPrice']),function ($qb) use ($data){
                return $qb->where('price', '<=', $data['maxPrice']);
            })
            ->when(isset($data['minKm']),function ($qb) use ($data){
                return $qb->where('odometer', '>=', $data['minKm']);
            })
            ->when(isset($data['maxKm']),function ($qb) use ($data){
                return $qb->where('odometer', '<=', $data['maxKm']);
            })
            ->select('products.*', 'car_models.name as model_name', 'car_marks.name as mark_name')
            ->skip($data['skip'])
            ->take($data['take'])
            ->get();
    }
}
