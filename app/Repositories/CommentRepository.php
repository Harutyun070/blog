<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 5/15/20
 * Time: 3:57 PM
 */

namespace App\Repositories;


use App\Contracts\CommentInterface;
use App\Models\Comment;

/**
 * Class CommentRepository
 * @package App\Repositories
 */
class CommentRepository implements CommentInterface
{

    /**
     * @var
     */
    protected $model;

    public function __construct(Comment $comment)
    {
        $this->model = $comment;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->where('product_id', $id)->with('user')->get();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function store($data)
    {
        return $this->model->create($data);
    }

}
