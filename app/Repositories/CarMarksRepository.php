<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 6/5/20
 * Time: 3:23 PM
 */

namespace App\Repositories;


use App\Contracts\CarMarksInterface;
use App\Models\CarMark;

class CarMarksRepository implements CarMarksInterface
{

    protected $model;

    public function __construct(CarMark $carMark)
    {
        $this->model = $carMark;
    }

    /**
     * @return mixed
     */
    public function getMarks()
    {
       return $this->model->get();
    }
}
