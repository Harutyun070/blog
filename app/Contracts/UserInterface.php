<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 4/27/20
 * Time: 8:47 PM
 */

namespace App\Contracts;

/**
 * Interface UserInterface
 * @package App\Http\Contracts
 */
interface UserInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function store($data);

    /**
     * @param $skip
     * @param $take
     * @return mixed
     */
    public function getAll($skip, $take);

    /**
     * @param $userId
     * @return mixed
     */
    public function index($userId);

    /**
     * @param $userId
     * @return mixed
     */
    public function show($userId);

    /**
     * @param $id
     * @param $user
     * @return mixed
     */
    public function update($id, $user);

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateOrCreateAddress($id, $data);
}
