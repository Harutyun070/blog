<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 4/25/20
 * Time: 8:15 PM
 */

namespace App\Contracts;

/**
 * Interface ProductsInterface
 * @package App\Contracts
 */
interface ProductsInterface
{
    /**
     * @param $skip
     * @param $take
     * @return mixed
     */
    public function getAll($skip, $take);

    /**
     * @param $data
     * @return mixed
     */
    public function store($data);

    /**
     * @param $productId
     * @return mixed
     */
    public function getByIdWithComments($productId);

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data);


    /**
     * @param $productId
     * @return mixed
     */
    public function destroy($productId);

    /**
     * @param $userId
     * @param $skip
     * @param $take
     * @return mixed
     */
    public function getUserProducts($userId, $skip, $take);

    /**
     * @param $data
     * @return mixed
     */
    public function getSearchingProducts($data);

    /**
     * @param $data
     * @return mixed
     */
    public function getByParams($data);
}
