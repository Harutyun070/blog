<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 6/1/20
 * Time: 2:46 PM
 */

namespace App\Contracts;

/**
 * Interface RatingInterface
 * @package App\Contracts
 */
interface RatingInterface
{
    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function store($userId,$data);

}
