<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 6/5/20
 * Time: 3:24 PM
 */

namespace App\Contracts;

/**
 * Interface CarMarksInterface
 * @package App\Contracts
 */
interface CarMarksInterface
{
    /**
     * @return mixed
     */
    public function getMarks();
}
