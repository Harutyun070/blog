<?php


namespace App\Contracts;

/**
 * Interface CommentInterface
 * @package App\Contracts
 */
interface  CommentInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $data
     * @return mixed
     */
    public function store($data);
}
