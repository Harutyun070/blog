<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 6/5/20
 * Time: 3:24 PM
 */

namespace App\Contracts;

/**
 * Interface CarModelsInterface
 * @package App\Contracts
 */
interface CarModelsInterface
{
    /**
     * @param $mark
     * @return mixed
     */
    public function getModels($mark);
}
