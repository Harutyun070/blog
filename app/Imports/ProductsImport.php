<?php

namespace App\Imports;

use App\Mail\AdminAddedProducts;
use App\Mail\ErrorAddingProducts;
use App\Models\Product;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\ImportFailed;

class ProductsImport implements ToModel,  WithHeadingRow, ShouldQueue, WithChunkReading, WithEvents
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'model_id'    => $row['model_id'],
            'price'       => $row['price'],
            'description' => $row['description'],
            'user_id'     => $row['user_id'],
            'category'    => $row['category'],
            'year'        => $row['year'],
            'odometr'     => $row['odometr'],
        ]);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            ImportFailed::class => function(ImportFailed $event) {
                $message = $event->getException()->getMessage();
                Mail::to('test@test.test')->send(new ErrorAddingProducts($message));
            },
            AfterImport::class => function() {
                Mail::to('example@dsadas.test')->send(new AdminAddedProducts());
            },
        ];
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 50;
    }
}
