<?php

namespace App\Imports;

use App\Mail\AdminAddedUsers;
use App\Mail\ErrorAddingUsers;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\ImportFailed;

/**
 * Class UsersImport
 * @package App\Imports
 */
class UsersImport implements ToModel, WithHeadingRow, ShouldQueue, WithChunkReading, WithEvents
{

    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new User([
            'name'  => $row['name'],
            'email' => $row['email'],
            'password' => "",
        ]);
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return 50;
    }


    /**
     * @return array
     */
    public function registerEvents(): array
    {

        return [
            ImportFailed::class => function(ImportFailed $event) {
                $message = $event->getException()->getMessage();
                Mail::to('test@test.test')->send(new ErrorAddingUsers($message));
            },
            AfterImport::class => function() {
                Mail::to('example@dsadas.test')->send(new AdminAddedUsers());
            },
        ];
    }
}
