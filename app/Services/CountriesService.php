<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 5/21/20
 * Time: 6:54 PM
 */


namespace App\Services {


    use PragmaRX\Countries\Package\Countries;

    /**
     * Class CountriesService
     * @package App\Services
     */
    class CountriesService
    {
        /**
         * @var Countries
         */
        protected $countries;

        /**
         * CountriesService constructor.
         * @param Countries $countries
         */
        public function __construct(Countries $countries)
        {
            $this->countries = $countries;
        }

        /**
         * @return Countries
         */
        public function getCountries()
        {
            return $this->countries->all()->pluck('name.common');
        }

        /**
         * @param $country
         * @return mixed
         * return cities data.
         */
        public function getCities($country)
        {
            $cities = $this->countries->where('name.common', $country)->first()
                ->hydrate('cities')
                ->cities->toArray();
            $data = [];
            foreach ($cities as $city) {
                $data[] = [
                    'name'  => $city['name'],
                    'lat'   => $city['latitude'],
                    'long'   => $city['longitude'],
                ];
            }
            return $data;
        }
    }
}
