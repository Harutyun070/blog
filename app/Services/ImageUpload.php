<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 4/24/20
 * Time: 5:21 PM
 */

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * Class ImageUpload
 * @package App\Services
 */
class ImageUpload
{
    /**
     * Upload image
     *
     * @param $file
     * @param $path
     * @return bool
     */
    public function productUpload($file,$path)
    {
        try {
            $imageName = $file->getClientOriginalName();
            $targetFile = $path . basename($imageName);
            if (!is_dir($targetFile)) {
                File::makeDirectory($path, 0777, true, true);
            }
            Storage::disk('public')->putFileAs($path, $file, $imageName);
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }
}
