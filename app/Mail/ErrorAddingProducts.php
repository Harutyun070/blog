<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class ErrorAddingProducts
 * @package App\Mail
 */
class ErrorAddingProducts extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    /**
     * @var
     */
    protected $message;

    /**
     * ErrorAddingProducts constructor.
     * @param $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@example.com', 'Mailtrap')
            ->subject('Error Products Adding')
            ->markdown('mails.error')
            ->with([
                'name' => 'New Mailtrap User',
                'link' => 'https://blog.test',
                'message' => $this->message
            ]);
    }
}
