<?php
/**
 * Created by PhpStorm.
 * User: antonyan
 * Date: 5/28/20
 * Time: 7:16 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class ErrorAddingUsers extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('mail@example.com', 'Mailtrap')
            ->subject('Mailtrap Confirmation')
            ->markdown('mails.error')
            ->with([
                'name' => 'New Mailtrap User',
                'link' => 'https://blog.test',
                'message' => $this->message
            ]);
    }
}
