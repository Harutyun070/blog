<?php


namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminAddedProducts extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('mail@example.com', 'Mailtrap')
            ->subject('Added Products')
            ->markdown('mails.email');
    }
}
