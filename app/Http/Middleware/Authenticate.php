<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Access\Gate;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

/**
 * Class Authenticate
 * @package App\Http\Middleware
 */
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->user()) {
            return response()->json([
                'status'          => 401,
                'authenticated'   => false,
                'success'         => false,
                'message'         => 'Please log in for more comfortably!'
            ]);
        }
    }
}
