<?php

namespace App\Http\Controllers\API;


use App\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserCreateRequest;
use App\Models\Role;
use Exception;

/**
 * Class RegistrationController
 * @package App\Http\Controllers
 */
class RegistrationController extends Controller
{

    /**
     * @var UserInterface
     */
    protected $userRepo;

    /**
     * RegistrationController constructor.
     * @param UserInterface $userRepo
     */
    public function __construct(UserInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Register user method.
     *
     * @param UserCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UserCreateRequest $request)
    {
        try {
            $data = $request->all();;
            if ($data) {
                $user = $this->userRepo->store($data);
                $role = Role::where('name', 'user')->first();
                $user->roles()->attach($role);
                return response()->json([
                    'success' =>  1,
                ]);
            }
            return response()->json([
                'success' => 0,
                'message' => 'Something went wrong!',
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ]);
        }
    }
}

