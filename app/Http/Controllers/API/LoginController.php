<?php

namespace App\Http\Controllers\API;

use App\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use Exception;
use Illuminate\Support\Facades\Auth;



/**
 * Class LoginController
 * @package App\Http\Controllers
 */
class LoginController extends Controller
{

    /**
     * @var UserInterface
     */
    protected $userRepo;

    /**
     * UserController constructor.
     * @param UserInterface $userRepo
     */
    public function __construct(UserInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     *return Auth user.
     *
     */
    public function authUser()
    {
        try {
            $authUser = Auth::user();
            if ($authUser){
                $authUser = $this->userRepo->index($authUser->id);
                return response()->json([
                    'success'  => 1,
                    'authUser' => $authUser,
                ]);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'You are not logged!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => $exception->getMessage()
            ]);
        }
    }


    /**
     * Check role login user method.
     *
     * @param UserLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function authenticate(UserLoginRequest $request)
    {
        try {
            $auth = $request->only('email', 'password');

            if (Auth::attempt($auth)) {
                $user = Auth::user();
                $accessToken = $user->createToken('authToken')->accessToken;
                if (\Gate::allows('isAdmin')) {

                    return response()->json([
                        'user'            => $user,
                        'access_token'    => $accessToken,
                        'success'         => true,
                        'message'         => 'Welcome Admin!'
                    ]);
                }
                return response()->json([
                    'user'          => $user,
                    'success'       => true,
                    'access_token'  => $accessToken,
                    'message'       => 'You are logged in!'
                ]);
            }
            return response()->json([
                'success'       => false,
                'message'       => 'Please Log in!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'errors'  => $exception->getMessage()
            ]);
        }
    }

    /**
     * User log out.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function logout()
    {

        try {
            $authUser = Auth::user();
            if ($authUser){
                $authUser->AauthAcessToken()->delete();
                \session()->flush();
                return response()->json([
                    'success' => true,
                    'message' => 'Logged Out!'
                ]);
            }
            return response()->json([
                'success' =>false,
                'error'   => 'You are already logged out!'
            ]);

        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'error'   => $exception->getMessage()
            ]);
        }
    }
}
