<?php

namespace App\Http\Controllers\API;

use App\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminEditRequest;
use App\Http\Requests\ImportRequest;
use App\Http\Requests\UserCreateRequest;
use App\Imports\ProductsImport;
use App\Imports\UsersImport;
use App\Models\Role;
use App\Models\User;
use App\Services\CountriesService;
use App\Services\ImageUpload;
use Exception;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;


class AdminController extends Controller
{
    /**
     * @var UserInterface
     */
    protected $userRepo, $countriesService;

    /**
     * AdminController constructor.
     * @param UserInterface $userRepo
     * @param CountriesService $countriesService
     */
    public function __construct(UserInterface $userRepo, CountriesService $countriesService)
    {
        $this->userRepo = $userRepo;
        $this->countriesService = $countriesService;
    }

    /**
     *
     * Show all user`s.
     * .
     */
    public function index()
    {
        try {

            $skip = \request()->input('skip', 0);
            $take = \request()->input('take', 10);
            $users = $this->userRepo->getAll($skip, $take);
            return response()->json([
                'success' => 1,
                'users' => $users
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0
            ]);
        }
    }

    /**
     * get States
     * @param $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStates($country)
    {
        $cities = $this->countriesService->getCities($country);
        return response()->json([
            'success' => 1,
            'cities' => $cities
        ]);
    }

    /**
     * User profile edit page for Admin.
     *
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($userId)
    {
        try {
            if ($userId) {
                $user = $this->userRepo->show($userId);
                $countries = $this->countriesService->getCountries();
                return response()->json([
                    'success'   => 1,
                    'user'      => $user,
                    'countries' => $countries
                ]);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'Something go wrong!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'error'   => $exception->getMessage()
            ]);
        }
    }

    /**
     * Admin create user method.
     *
     * @param UserCreateRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserCreateRequest $request, User $user)
    {
        try {
            if ($this->authorize('create', $user)) {
                $data = $request->except('file');
                if ($data) {
                    $user = $this->userRepo->store($data);
                    $role = Role::where('name', 'user')->first();
                    $user->roles()->attach($role);
                    if ($request->hasFile('file')) {
                        $file = $request->file;
                        $path = "/users/$user->id/";
                        $fileUploadService = new ImageUpload();
                        $fileUploadService->productUpload($file, $path);
                    }
                    return response()->json([
                        'success' => 1,
                        'message' => 'Profile successfully added!'
                    ]);
                }
                return response()->json([
                    'success' => 0,
                    'errors'  => 'Error creating profile!'
                ]);
            }
            return response()->json([
                'success' => 0,
                'errors'  => 'You are not administrator'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'errors'  => $exception->getMessage()
            ]);
        }
    }

    /**
     * Admin update users profile method.
     *
     * @param AdminEditRequest $request
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AdminEditRequest $request, $userId)
    {
        try {
            if ($this->authorize('update', $request->user())) {
                if ($userId) {
                    $user = $request->user;
                    if ($request->hasFile('file')) {
                        $file = $request->file;
                        $path = "/users/$userId/";
                        $fileUploadService = new ImageUpload();
                        $fileUploadService->productUpload($file, $path);
                        $request->except('file');
                        $user = $request->user;
                        $img = $request->image;
                        $user['image'] = $img;
                    }
                    $userData = $this->userRepo->update($userId, $user);
                    if ($request->address) {
                        $address = $request->address;
                        $this->userRepo->updateOrCreateAddress($userId, $address);
                    }
                    return response()->json([
                        'success' => 1,
                        'user'    => $userData,
                        'message' => 'Profile updated successfully!'
                    ]);
                }
                return response()->json([
                    'success' => 0,
                    'error'   => 'You are not administrator'
                ]);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'Anything go wrong!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Admin delete users profile method.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            if ($id) {
                $this->userRepo->destroy($id);
                return response()->json([
                    'success' => 1,
                    'message' => 'User successfully deleted'
                ]);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'User does not deleted!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'message' => 'User does not deleted!',
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param ImportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importUser(ImportRequest $request)
    {
        try {
            if ($request->hasFile('importFileUsers')) {
                Excel::queueImport(new UsersImport, $request->file('importFileUsers'));
                return response()->json([
                    'success' => 1,
                    'message' => 'Uploading!!!'
                ], 200);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'Something go wrong!',
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'error'   => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param ImportRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importProduct(ImportRequest $request)
    {
        try {
            if ($request->hasFile('importFileProducts')) {
                Excel::queueImport(new ProductsImport, $request->file('importFileProducts'));
                return response()->json([
                    'success' => 1,
                    'message' => 'Uploading!!!'
                ], 200);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'Something go wrong!',
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'error'   => $exception->getMessage()
            ]);
        }
    }
}
