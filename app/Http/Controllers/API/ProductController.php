<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Contracts\ProductsInterface;
use App\Http\Requests\FilterRequest;
use App\Http\Requests\ProductsRequest;
use App\Http\Requests\SearchRequest;
use Exception;
use App\Services\ImageUpload;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers
 */
class ProductController extends Controller
{
    /**
     * @var ProductsInterface
     */
    protected $productsRepo;

    /**
     * ProductController constructor.
     * @param ProductsInterface $productsRepo
     */
    public function __construct(ProductsInterface $productsRepo)
    {
        $this->productsRepo = $productsRepo;
    }

    /**
     * Show all products.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $skip = \request()->input('skip', 0);
            $take = \request()->input('take', 10);
            $products = $this->productsRepo->getAll($skip, $take);
            return response()->json([
                'success'  => true,
                'products' => $products
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'error'    => $exception->getMessage()
            ]);
        }
    }

    /**
     * Show that product.
     *
     * @param $productId
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($productId)
    {

        try {
            if ($productId) {
                $product = $this->productsRepo->getByIdWithComments($productId);
                $ratings = $product->ratings;
                if($ratings){
                    $ratingSummary = $ratings->sum('rate');
                    $countSummary = $ratings->count();
                    return response()->json([
                        'success'       => true,
                        'product'       => $product,
                        'ratingSummary' => $ratingSummary,
                        'countSummary'  => $countSummary
                    ]);
                }
                return response()->json([
                    'success' => true,
                    'product' => $product,
                ]);
            }
            return redirect(route('product.index'));
        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'error'   => 'Something go wrong!!',
                'errors'  => $exception->getMessage()
            ]);
        }
    }

    /**
     *  Update product method.
     *
     * @param ProductsRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(ProductsRequest $request)
    {
        try {
            $data = $request->all();
            $id = $request->id;
            if ($data) {
                if ($request->hasFile('file')) {
                    $file = $request->file;
                    $path = "/products/$id/";
                    $fileUploadService = new ImageUpload();
                    $fileUploadService->productUpload($file, $path);
                    $data = $request->except('file');
                }
                $productId = $this->productsRepo->update($id, $data);
                return response()->json([
                    'productId' => $productId,
                    'message' => 'Product updated successfully'
                ]);
            }
            return redirect()->back()->with('error', 'Product does not update!');
        } catch (Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Create product method.
     *
     * @param ProductsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductsRequest $request)
    {
        try {
//            dd($request->all());
            $data = $request->except('image');
            if ($data) {
                $data['user_id'] = $request->user()->id;
                $product = $this->productsRepo->store($data);
                if ($request->hasFile('file')) {
                    $productId = $product->id;
                    $file = $request->file;
                    $path = "/products/$productId/";
                    $fileUploadService = new ImageUpload();
                    $fileUploadService->productUpload($file, $path);
                }
                return response()->json([
                    'success'   => 1,
                    'message'   => 'Your car successfully added!',
                ]);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'Something went wrong!',
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Delete product method.
     *
     * @param $productId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function destroy($productId)
    {
        try {
            if ($productId) {
                $this->productsRepo->destroy($productId);
                return redirect()->route('product.index');
            }
            return redirect()->back();
        } catch (Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * Show user`s product or products page.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserProducts()
    {
        try {
            $skip = \request()->input('skip', 0);
            $take = \request()->input('take', 10);
            $userId = \auth()->user()->id;
            if ($userId) {
                $products = $this->productsRepo->getUserProducts($userId, $skip, $take);
                return response()->json([
                    'success' => true,
                    'products' => $products,
                    'message' => 'Your Products!'
                ]);
            }
            return response()->json([
                'error' => 'You are not authenticated!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'error' => $exception->getMessage()
            ]);
        }
    }

    /**
     * searching Products.
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSearchingProducts(SearchRequest $request)
    {
        try{
            $data = $request->data;
            if ($data){
                $products = $this->productsRepo->getSearchingProducts($data);
                return response()->json([
                    'success'  => true,
                    'products' => $products
                ]);
            }
            return response()->json([
                'success' => false,
                'error'   => 'Something go wrong!'
            ]);
        }catch(Exception $exception){
            return response()->json([
                'success' => false,
                'error'   => $exception->getMessage()
            ]);
        }
    }

    /**
     * @param FilterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByParams(FilterRequest $request)
    {
        $data = $request->all();
        if($data){
           $products = $this->productsRepo->getByParams($data);
                if(count($products)){
                    return response()->json([
                        'success'  => true,
                        'products' => $products
                    ]);
                }else{
                    return response()->json([
                        'success'  => true,
                        'products' => $products,
                        'message'  => 'Data Missing!'
                    ]);
                }
        }
        return response()->json([
            'error'   => 'Something go wrong!!!'
        ]);
    }
}
