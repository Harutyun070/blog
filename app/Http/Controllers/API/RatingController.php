<?php

namespace App\Http\Controllers\API;

use App\Events\RatingEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\RatingRequest;
use App\Models\Rating;
use App\Repositories\RatingRepository;
use Exception;
use Illuminate\Http\Request;

class RatingController extends Controller
{

    protected $ratingRepo;

    /**
     * RatingController constructor.
     * @param RatingRepository $ratingRepo
     */
    public function __construct(RatingRepository $ratingRepo)
    {
        $this->ratingRepo = $ratingRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RatingRequest $request
     * @return void
     */
    public function store(RatingRequest $request)
    {
        try{
            $data = $request->all();
            $userId = auth()->user()->id;
            $rate = $this->ratingRepo->store($userId,$data);
            broadcast(new RatingEvent($rate));
            $rating = $rate->rate;
            return response()->json([
                'success' => true,
                'message' => 'Your rate for this Car are'.' '.$rating
            ]);
        }catch (Exception $exception){
            return response()->json([
                'success'      => false,
                'errorMessage' => 'Something go Wrong!!!',
                'error'        => $exception->getMessage()
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Rating $rating
     * @return void
     */
    public function show(Rating $rating)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Rating $rating
     * @return void
     */
    public function edit(Rating $rating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Rating $rating
     * @return void
     */
    public function update(Request $request, Rating $rating)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Rating $rating
     * @return void
     */
    public function destroy(Rating $rating)
    {
        //
    }
}
