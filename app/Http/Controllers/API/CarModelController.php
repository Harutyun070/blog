<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarModelRequest;
use App\Repositories\CarModelsRepository;
use Exception;

class CarModelController extends Controller
{
    /**
     * @var CarModelsRepository
     */
    protected $carModelsRepo;

    /**
     * CarModelController constructor.
     * @param CarModelsRepository $carModelsRepository
     */
    public function __construct(CarModelsRepository $carModelsRepository)
    {
        $this->carModelsRepo = $carModelsRepository;
    }

    public function getModels(CarModelRequest $request)
    {
        try{
            $markId = $request->mark_id;
            if($markId){
                $models = $this->carModelsRepo->getModels($markId);
                return response()->json([
                    'success' => true,
                    'models'  => $models
                ]);
            }
            return response()->json([
                'success' => false,
                'error'   => 'Something go wrong!!!'
            ]);
        }catch(Exception $exception){
            return response()->json([
                'success' => false,
                'error'   => $exception->getMessage()
            ]);
        }
    }
}
