<?php

namespace App\Http\Controllers\API;

use App\Events\CommentEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Repositories\CommentRepository;
use Exception;

/**
 * Class CommentController
 * @package App\Http\Controllers\API
 */
class CommentController extends Controller
{
    /**
     * @var
     */
    protected $repo;

    /**
     * CommentController constructor.
     * @param CommentRepository $commentRepository
     */
    public function __construct(CommentRepository $commentRepository)
    {
        $this->repo = $commentRepository;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CommentRequest $request)
    {
        try {
            $data = $request->all();
            $comment = $this->repo->store($data);
            $comment->load('user');
            broadcast(new CommentEvent($comment));
            return response()->json([
                'success' => 'Your comment added!',
            ]);
        } catch (Exception $exception){
           return response()->json([
               'success'=> false,
               'error'  => $exception->getMessage()
           ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
