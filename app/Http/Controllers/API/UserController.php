<?php

namespace App\Http\Controllers\API;

use App\Contracts\UserInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserEditRequest;
use App\Services\ImageUpload;
use Exception;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */
class UserController extends Controller
{

    /**
     * @var UserInterface
     */
    protected $userRepo;

    /**
     * UserController constructor.
     * @param UserInterface $userRepo
     */
    public function __construct(UserInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     *Show user`s update page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show()
    {
        try {
            $user = Auth::user();
            return view('user.show', compact('user'));
        } catch (Exception $exception) {
            return view('error.error');
        }
    }

    /**
     * Update profile method.
     *
     * @param UserEditRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function update(UserEditRequest $request)
    {
        try {
            $data = $request->all();
            $id = $request->id;
            if ($data) {
                if ($request->hasFile('imageName')) {
                    $file = $request->imageName;
                    $path = "/users/$id/";
                    $fileUploadService = new ImageUpload();
                    $fileUploadService->productUpload($file, $path);
                    $data = $request->except('imageName');
                }
                $this->userRepo->update($id, $data);
            }
            return redirect(route('product.index'))->with('success', 'Profile updated successfully!');
        } catch (Exception $exception) {
            return view('error.error');
        }
    }

    /**
     * User delete profile method.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            if ($id) {
                $this->userRepo->destroy($id);
                Auth::logout();
                return response()->json([
                    'success' => 1,
                    'message' => 'Your profile deleted!'
                ]);
            }
            return response()->json([
                'success' => 0,
                'error'   => 'Something went wrong!'
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'success' => 0,
                'error'   =>$exception->getMessage()
            ]);
        }
    }
}
