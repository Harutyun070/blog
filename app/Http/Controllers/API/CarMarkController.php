<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\CarMarksRepository;

/**
 * Class CarMarkController
 * @package App\Http\Controllers\API
 */
class CarMarkController extends Controller
{
    /**
     * @var CarMarksRepository
     */
    protected $carMarksRepo;

    /**
     * CarMarkController constructor.
     * @param CarMarksRepository $repo
     */
    public function __construct(CarMarksRepository $repo)
    {
        $this->carMarksRepo = $repo;

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMarks()
    {
        $marks = $this->carMarksRepo->getMarks();
        return response()->json([
            'success' => true,
            'marks'   => $marks
        ]);
    }

}
