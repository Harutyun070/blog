<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserEditRequest
 * @package App\Http\Requests
 */
class UserEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = auth()->user();
        return [
            'name'     => 'required|',
            'email'    => 'required|unique:users,email,' . $user->id . '|max:15',
            'password' => 'required|confirmed|min:8',
            'image'    => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ];
    }

    /**
     * Get validation after rules validation.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        if (!$validator->fails()) {
            $input = $this->except('_token', 'password_confirmation');
            if ($this->hasFile('imageName')) {
                $img = $this->imageName->getClientOriginalName();
                $input['image'] = $img;
            }
            if ($input['password']) {
                $input['password'] = bcrypt($input['password']);
            }
            $this->replace($input);
        }
        return $validator;
    }
}
