<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AdminEditRequest
 * @property mixed address
 * @property mixed file
 * @property mixed user
 * @package App\Http\Requests
 */
class AdminEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user.name'       => 'required|min:3|max:10',
            'address.country' => 'required',
            'address.city'    => 'required',
            'address.lat'     => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'address.long'    => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ];
    }

    /**
     * Get validation after rules validation.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        if (!$validator->fails()) {
            $input = $this->except('_token', 'user.address');
            if ($this->hasFile('file')) {
                $img = $this->file->getClientOriginalName();
                $input['image'] = $img;
            }
            $this->replace($input);
        }
        return $validator;
    }
}
