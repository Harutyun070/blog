<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ProductsRequest
 * @package App\Http\Requests
 */
class ProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mark_id'      => 'required',
            'model_id'     => 'required|filled|min:1',
            'price'        => 'required|numeric',
            'description'  => 'required|min:5|max:255',
            'category'     => 'required|max:10',
            'odometer'     => 'required|numeric|min:1|max:999999',
            'year'         => 'required|numeric|min:1950|max:2020',
        ];
    }

    /**
     * Get validation after rules validation.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        if (!$validator->fails()) {
            $input = $this->except('_method', '_token');
            if ($this->hasFile('file')) {
                $img = $this->file->getClientOriginalName();
                $input['image_name'] = $img;
            }
            $this->replace($input);
        }
        return $validator;
    }

    public function messages()
    {
        return [
            'model_id' => [
                'required' => 'The model field is required!'
            ],
            'mark_id' => [
                'required' => 'The mark field is required!'
            ]
        ];
    }
}
