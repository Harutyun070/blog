<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UserCreateRequest
 * @property mixed imageName
 * @package App\Http\Requests
 */
class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'email'    => 'required|unique:users|max:20',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get validation after rules validation.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        if (!$validator->fails()) {
            $input = $this->except('_token', 'password_confirmation');
            if ($this->hasFile('imageName')) {
                $img = $this->imageName->getClientOriginalName();
                $input['image'] = $img;
            }
            if ($input['password']) {
                $input['password'] = bcrypt($input['password']);
            }
            $this->replace($input);
        }
        return $validator;
    }
}
