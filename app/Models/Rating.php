<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Rating
 * @package App\Models
 */
class Rating extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['rate', 'count', 'user_id', 'product_id'];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
