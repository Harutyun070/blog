<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Address
 * @package App\Models
 */
class Address extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country', 'city', 'addressable_id', 'addressable_type', 'lat', 'long'];


    /**
     * Get the owning addressable model.
     */
    public function addressable()
    {
        return $this->morphTo();
    }

}
