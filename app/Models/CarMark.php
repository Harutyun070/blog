<?php

namespace App\Models;

use App\CarModel;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CarMark
 * @package App\Models
 */
class CarMark extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function models()
    {
        return $this->hasMany(CarModel::class, 'mark_id', 'mark_id');
    }
}
