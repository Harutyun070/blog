<?php

namespace App;

use App\Models\CarMark;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CarModel
 * @package App
 */
class CarModel extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mark()
    {
       return $this->belongsTo(CarMark::class, 'mark_id', 'mark_id');
    }
}
