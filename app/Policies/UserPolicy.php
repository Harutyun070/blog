<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

/**
 *
 * Check user role can use this method.
 *
 * Class UserPolicy
 * @package App\Policies
 */
class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Check have access to create user.
     *
     * @param User $user
     * @return Response
     */
    public function create(User $user)
    {
        if ($user->roles() && $user->roles()->name == 'admin') {
            return Response::allow();
        }
        return Response::deny();
    }

    /**
     *
     * Check have access to update user.
     *
     * @param User $user
     * @return Response
     */
    public function update(User $user)
    {
        if ($user->roles()->first() && $user->roles()->first()->name == 'admin') {
            return Response::allow();
        }
        return Response::deny();
    }
}
