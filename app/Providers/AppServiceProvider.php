<?php

namespace App\Providers;

use App\Contracts\ProductsInterface;
use App\Contracts\UserInterface;
use App\Repositories\ProductsRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            ProductsInterface::class,
            ProductsRepository::class
        );
        $this->app->bind(
            UserInterface::class,
            UserRepository::class
        );
    }
}
