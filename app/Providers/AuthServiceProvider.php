<?php

namespace App\Providers;

use App\Policies\UserPolicy;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

/**
 * Class AuthServiceProvider
 * @package App\Providers
 */
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     * Define user role.
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function ($router) {
            $router->forAccessTokens();
        });

        Passport::tokensExpireIn(Carbon::now()->addDays(1));

        Passport::refreshTokensExpireIn(Carbon::now()->addDays(10));

        Gate::define('isAdmin', function ($user) {
            return $user && $user->roles->first() && $user->roles->first()->name == 'admin';
        });

        Gate::define('isModerator', function ($user) {
            return $user && $user->roles->first() && $user->roles->first()->name == 'moderator';
        });

        Gate::define('isUser', function ($user) {
            return $user && $user->roles->first() && $user->roles->first()->name == 'user';
        });
    }
}
