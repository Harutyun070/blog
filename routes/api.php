<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/products', 'API\ProductController@index');

Route::get('/product/search', 'API\ProductController@getSearchingProducts');

Route::get('/product/{id}', 'API\ProductController@show');

Route::post('/login', 'API\LoginController@authenticate');

Route::get('/getMarks', 'API\CarMarkController@getMarks');

Route::get('/getModels', 'API\CarModelController@getModels');

Route::get('/getByParams', 'API\ProductController@getByParams');

Route::post('/register', 'API\RegistrationController@register');

Route::group(['middleware' => ['web', 'auth:api']], function () {

    Route::get('/auth', 'API\LoginController@authUser');

    Route::get('/allUsers', 'API\AdminController@index');

    Route::get('/user/edit/{id}', 'API\AdminController@edit');

    Route::post('/user/edit/{userId}', 'API\AdminController@update');

    Route::delete('/user/{userId}/delete', 'API\AdminController@destroy');

    Route::get('/states/{name}', 'API\AdminController@getStates');

    Route::get('/userProducts', 'API\ProductController@getUserProducts');

    Route::post('/product/create', 'API\ProductController@store');

    Route::post('/product/edit/{id}', 'API\ProductController@update');

    Route::post('/product/{id}/addComment', 'API\CommentController@store');

    Route::post('/users/import','API\AdminController@importUser');

    Route::post('/product/{id}/addRate', 'API\RatingController@store');

    Route::post('/products/import','API\AdminController@importProduct');

    Route::post('/logOut', 'API\LoginController@logout');

});





