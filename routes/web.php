<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/login', 'LoginController@show')->name('login.show');
//
//Route::get('/register', 'RegistrationController@show')->name('register');
//
//Route::post('/login', 'LoginController@authenticate')->name('user.login');
//
//Route::post('/register', 'RegistrationController@register')->name('user.register');
//
//Route::middleware(['auth'])->group(function () {
//
//    Route::get('/', 'HomeController@index');
//
//    Route::resource('/product', 'ProductController');
//
//    Route::group(['prefix' => '/admin/users'], function () {
//
//        Route::get('/', 'AdminController@index')->name('admin.users');
//
//        Route::get('/profile', 'AdminController@show')->name('profile.show');
//
//        Route::get('/create', 'AdminController@create')->name('admin.create');
//
//        Route::post('/store', 'AdminController@store')->name('admin.store');
//
//        Route::get('/{id}', 'AdminController@edit')->name('admin.edit');
//
//        Route::post('/{id}', 'AdminController@update')->name('admin.update');
//
//        Route::delete('/{id}/destroy', 'AdminController@destroy')->name('admin.destroy');
//
//    });
//
//    Route::group(['prefix' => '/user'], function () {
//
//        Route::get('/profile', 'UserController@show')->name('user.show');
//
//        Route::post('/profile/update', 'UserController@update')->name('user.update');
//
//        Route::get('/products', 'ProductController@getUserProducts')->name('user.products');
//
//        Route::delete('/{id}', 'UserController@destroy')->name('user.destroy');
//
//    });
//
//    Route::get('/logout', 'LoginController@logout')->name('logout');
//
//});


Route::get('/{any}', 'HomeController@index')->where('any','.*');






