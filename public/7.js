(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pages/product/productPage.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "productPage",
  data: function data() {
    return {
      projectUrl: location.origin,
      product: [],
      content: [],
      comments: [],
      rating: 0,
      message: '',
      error: ''
    };
  },
  created: function created() {
    this.index();
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['authUser'])),
  mounted: function mounted() {
    var _this = this;

    Echo.channel('my-channel').listen('.CommentEvent', function (data) {
      if (data && data.comment) {
        _this.comments.push(data.comment);

        _this.content = [];
      }
    });
    Echo.channel('rate').listen('.RatingEvent', function (data) {
      _this.index();
    });
  },
  methods: {
    index: function index() {
      var _this2 = this;

      var id = this.$route.params.id;
      axios.get("/api/product/".concat(id)).then(function (response) {
        _this2.product = response.data.product;
        _this2.comments = _this2.product.comment;

        if (response.data.ratingSummary && response.data.countSummary) {
          _this2.rating = response.data.ratingSummary / response.data.countSummary;
        }
      });
    },
    addComment: function addComment() {
      var id = this.$route.params.id;
      axios.post("/api/product/".concat(id, "/addComment"), {
        content: this.content,
        product_id: this.product.id
      }).then(function (response) {// console.log(response)
      });
    },
    setRating: function setRating() {
      var _this3 = this;

      var id = this.$route.params.id;
      axios.post("/api/product/".concat(id, "/addRate"), {
        rate: this.rating,
        product_id: this.product.id
      }).then(function (response) {
        if (response.data && response.data.success) {
          _this3.message = response.data.message;
        }
      })["catch"](function (errors) {
        _this3.error = response.data.error;
      });
    },
    getCounting: function getCounting() {}
  },
  watch: {
    $route: function $route() {
      this.index();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.commentImg[data-v-08748210]{\n    max-width: 25%\n}\n.user_name[data-v-08748210]{\n    font-size: 20px;\n    font-weight: bold;\n}\n.comments-list .media[data-v-08748210]{\n    border-bottom: 1px dotted #ccc;\n}\nbody[data-v-08748210] {\n    font-family: 'open sans';\n    overflow-x: hidden;\n}\nimg[data-v-08748210] {\n    max-width: 100%;\n}\n.comment[data-v-08748210]{\n    text-transform: uppercase;\n    width: 150px;\n    height: 40px;\n    border-radius: 80px;\n    font-size: 16px;\n    line-height: 35px;\n    text-align: center;\n    border: 3px solid #e6de08;\n    display: block;\n    text-decoration: none;\n    margin: 5px auto 1px auto;\n    color: #000;\n    overflow: hidden;\n    position: relative;\n    background-color: #9561e2;\n}\n.ad-btn[data-v-08748210] {\n    text-transform: uppercase;\n    width: 150px;\n    height: 40px;\n    border-radius: 80px;\n    font-size: 16px;\n    line-height: 35px;\n    text-align: center;\n    border: 3px solid #e6de08;\n    display: block;\n    text-decoration: none;\n    margin: 20px auto 1px auto;\n    color: #000;\n    overflow: hidden;\n    position: relative;\n    background-color: blue;\n}\n.preview[data-v-08748210] {\n    display: flex;\n    flex-direction: column;\n}\n@media screen and (max-width: 996px) {\n.preview[data-v-08748210] {\n        margin-bottom: 20px;\n}\n}\n.preview-pic[data-v-08748210] {\n    flex-grow: 1;\n}\n.preview-thumbnail.nav-tabs li[data-v-08748210] {\n    width: 18%;\n    margin-right: 2.5%;\n}\n.preview-thumbnail.nav-tabs li img[data-v-08748210] {\n    max-width: 100%;\n    display: block;\n}\n.preview-thumbnail.nav-tabs li a[data-v-08748210] {\n    padding: 0;\n    margin: 0;\n}\n.preview-thumbnail.nav-tabs li[data-v-08748210]:last-of-type {\n    margin-right: 0;\n}\n.tab-content[data-v-08748210] {\n    overflow: hidden;\n}\n.tab-content img[data-v-08748210] {\n    width: 100%;\n    -webkit-animation-name: opacity-data-v-08748210;\n    animation-name: opacity-data-v-08748210;\n    -webkit-animation-duration: .3s;\n    animation-duration: .3s;\n}\n.card[data-v-08748210] {\n    margin-top: 50px;\n    background: #eee;\n    padding: 3em;\n    line-height: 1.5em;\n}\n@media screen and (min-width: 997px) {\n.wrapper[data-v-08748210] {\n        display: flex;\n}\n}\n.details[data-v-08748210] {\n    display: flex;\n    flex-direction: column;\n}\n.product-title[data-v-08748210], .price[data-v-08748210], .sizes[data-v-08748210], .colors[data-v-08748210] {\n    text-transform: UPPERCASE;\n    font-weight: bold;\n}\n.checked[data-v-08748210], .price span[data-v-08748210] {\n    color: #ff9f1a;\n}\n.product-title[data-v-08748210], .rating[data-v-08748210], .product-description[data-v-08748210], .price[data-v-08748210], .vote[data-v-08748210], .sizes[data-v-08748210] {\n    margin-bottom: 15px;\n}\n.product-title[data-v-08748210] {\n    margin-top: 0;\n}\n@-webkit-keyframes opacity-data-v-08748210 {\n0% {\n        opacity: 0;\n        transform: scale(3);\n}\n100% {\n        opacity: 1;\n        transform: scale(1);\n}\n}\n@keyframes opacity-data-v-08748210 {\n0% {\n        opacity: 0;\n        transform: scale(3);\n}\n100% {\n        opacity: 1;\n        transform: scale(1);\n}\n}\ntextarea[data-v-08748210]{\n    margin-top: 15px;\n    margin-bottom: 0px;\n    height: 106px;\n    resize: none;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=template&id=08748210&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pages/product/productPage.vue?vue&type=template&id=08748210&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "card mt-5" }, [
        _c("div", { staticClass: "container-fliud" }, [
          _c("div", { staticClass: "wrapper row" }, [
            _c("div", { staticClass: "preview col-md-6" }, [
              _c("div", { staticClass: "preview-pic tab-content" }, [
                _c(
                  "div",
                  { staticClass: "tab-pane active", attrs: { id: "pic-1" } },
                  [
                    _vm.product.image_name
                      ? _c("img", {
                          staticClass: "img-fluid",
                          attrs: {
                            src:
                              _vm.projectUrl +
                              "/storage/uploads/products/" +
                              _vm.product.id +
                              "/" +
                              _vm.product.image_name,
                            alt: "Alternate Text"
                          }
                        })
                      : _c("img", {
                          staticClass: "card-img-top",
                          attrs: {
                            src:
                              _vm.projectUrl +
                              "/images/default/product/car-default.jpg"
                          }
                        })
                  ]
                ),
                _vm._v(" "),
                _vm._m(0),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _vm._m(2)
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "details col-md-6" }, [
              _c("h3", { staticClass: "product-title" }, [
                _vm._v(_vm._s(_vm.product.name))
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "rating" }, [
                _vm._m(3),
                _vm._v(" "),
                _c("span", { staticClass: "review-no" }, [
                  _vm._v(_vm._s(_vm.product.category))
                ])
              ]),
              _vm._v(" "),
              _c("h4", { staticClass: "price" }, [
                _vm._v("current price: "),
                _c("span", [_vm._v("$" + _vm._s(_vm.product.price))])
              ]),
              _vm._v(" "),
              _c("h5", [
                _vm._v("Odometer:\n                            "),
                _c(
                  "span",
                  {
                    attrs: { "data-toggle": "tooltip", title: "Not In store" }
                  },
                  [_vm._v(_vm._s(_vm.product.odometr) + " Km")]
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "product-description" }, [
                _vm._v(_vm._s(_vm.product.description))
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c("star-rating", {
              on: { "rating-selected": _vm.setRating },
              model: {
                value: _vm.rating,
                callback: function($$v) {
                  _vm.rating = $$v
                },
                expression: "rating"
              }
            }),
            _vm._v(" "),
            _vm.message !== ""
              ? _c(
                  "div",
                  {
                    staticClass: "alert alert-success",
                    attrs: { role: "alert" }
                  },
                  [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.message) +
                        "\n                "
                    )
                  ]
                )
              : _vm._e(),
            _vm._v(" "),
            _vm.error !== ""
              ? _c(
                  "div",
                  {
                    staticClass: "alert alert-danger",
                    attrs: { role: "alert" }
                  },
                  [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.error) +
                        "\n                "
                    )
                  ]
                )
              : _vm._e()
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("div", [
        _c("div", { staticClass: "row" }, [
          _c(
            "div",
            { staticClass: "col-md-8" },
            [
              _c("div", { staticClass: "page-header" }, [
                _c("h1", [
                  _c("small", { staticClass: "pull-right" }, [
                    _vm._v(_vm._s(_vm.comments.length))
                  ]),
                  _vm._v(" Comments ")
                ])
              ]),
              _vm._v(" "),
              _vm._l(_vm.comments, function(comment) {
                return _c("div", { staticClass: "comments-list" }, [
                  _c("div", { staticClass: "media" }, [
                    _c(
                      "a",
                      { staticClass: "media-left", attrs: { href: "#" } },
                      [
                        comment.user.image
                          ? _c("img", {
                              staticClass: "commentImg",
                              attrs: {
                                src:
                                  "../storage/uploads/users/" +
                                  comment.user.id +
                                  "/" +
                                  comment.user.image
                              }
                            })
                          : _vm._e()
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "media-body" }, [
                      _c("h4", { staticClass: "media-heading user_name" }, [
                        _vm._v(_vm._s(comment.user.name))
                      ]),
                      _vm._v(
                        "\n                                " +
                          _vm._s(comment.content) +
                          "\n                                "
                      )
                    ])
                  ])
                ])
              })
            ],
            2
          )
        ]),
        _vm._v(" "),
        Object.keys(_vm.authUser).length
          ? _c(
              "form",
              {
                attrs: { method: "post" },
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.addComment($event)
                  }
                }
              },
              [
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.content,
                      expression: "content"
                    }
                  ],
                  attrs: { placeholder: "Add your comment" },
                  domProps: { value: _vm.content },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.content = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("button", { staticClass: "comment" }, [
                  _vm._v("Add Comment")
                ])
              ]
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "action" })
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "tab-pane", attrs: { id: "pic-3" } }, [
      _c("img", { attrs: { src: "http://placekitten.com/400/252" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "tab-pane", attrs: { id: "pic-4" } }, [
      _c("img", { attrs: { src: "http://placekitten.com/400/252" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "tab-pane", attrs: { id: "pic-5" } }, [
      _c("img", { attrs: { src: "http://placekitten.com/400/252" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "stars" }, [
      _c("span", { staticClass: "fa fa-star checked" }),
      _vm._v(" "),
      _c("span", { staticClass: "fa fa-star checked" }),
      _vm._v(" "),
      _c("span", { staticClass: "fa fa-star checked" }),
      _vm._v(" "),
      _c("span", { staticClass: "fa fa-star" }),
      _vm._v(" "),
      _c("span", { staticClass: "fa fa-star" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/pages/product/productPage.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/pages/product/productPage.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _productPage_vue_vue_type_template_id_08748210_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./productPage.vue?vue&type=template&id=08748210&scoped=true& */ "./resources/js/components/pages/product/productPage.vue?vue&type=template&id=08748210&scoped=true&");
/* harmony import */ var _productPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./productPage.vue?vue&type=script&lang=js& */ "./resources/js/components/pages/product/productPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css& */ "./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _productPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _productPage_vue_vue_type_template_id_08748210_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _productPage_vue_vue_type_template_id_08748210_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "08748210",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pages/product/productPage.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pages/product/productPage.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/pages/product/productPage.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./productPage.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css& ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=style&index=0&id=08748210&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_style_index_0_id_08748210_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/pages/product/productPage.vue?vue&type=template&id=08748210&scoped=true&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/pages/product/productPage.vue?vue&type=template&id=08748210&scoped=true& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_template_id_08748210_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./productPage.vue?vue&type=template&id=08748210&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/product/productPage.vue?vue&type=template&id=08748210&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_template_id_08748210_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_productPage_vue_vue_type_template_id_08748210_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);