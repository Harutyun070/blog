(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/user/users.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pages/user/users.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "users",
  data: function data() {
    return {
      users: [],
      busy: false,
      message: ''
    };
  },
  methods: {
    index: function index() {
      var _this = this;

      this.busy = true;
      var data = {
        params: {
          skip: this.users.length,
          take: 10
        }
      }; // let loader = Vue.$loading.show()

      axios.get("/api/allUsers", data).then(function (response) {
        // this.loader.hide()
        if (response.data) {
          response.data.users.forEach(function (item) {
            _this.users.push(item);

            _this.busy = false;
          }); // console.log(this.$route.params)
          // this.message = this.$route.params.message
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/user/users.vue?vue&type=template&id=47c62e8c&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/pages/user/users.vue?vue&type=template&id=47c62e8c& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      directives: [
        {
          name: "infinite-scroll",
          rawName: "v-infinite-scroll",
          value: _vm.index,
          expression: "index"
        }
      ],
      attrs: {
        "infinite-scroll-disabled": "busy",
        "infinite-scroll-distance": "10"
      }
    },
    [
      _c("div", { staticClass: "container" }, [
        _c("section", { staticClass: "container mt-4 mb-4" }, [
          _c("div", { staticClass: "container" }, [
            _c(
              "div",
              { staticClass: "row mb-3" },
              _vm._l(_vm.users, function(user) {
                return _c("div", { staticClass: "col-md-6" }, [
                  _c("div", { staticClass: "d-flex flex-row border rounded" }, [
                    _c("div", { staticClass: "p-0 w-25" }, [
                      _c("img", {
                        staticClass: "img-thumbnail border-0",
                        attrs: {
                          src: user.image
                            ? "storage/uploads/users/" +
                              user.id +
                              "/" +
                              user.image
                            : "storage/uploads/users/default/downd.png"
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "pl-3 pt-2 pr-2 pb-2 w-75 border-left" },
                      [
                        _c("h4", { staticClass: "text-primary" }, [
                          _vm._v(" " + _vm._s(user.name))
                        ]),
                        _vm._v(" "),
                        _c("h5", { staticClass: "text-info" }, [
                          _vm._v(_vm._s(user.email))
                        ]),
                        _vm._v(" "),
                        _vm._m(0, true),
                        _vm._v(" "),
                        _c(
                          "p",
                          { staticClass: "text-right m-0" },
                          [
                            _c(
                              "router-link",
                              {
                                staticClass: "btn btn-primary",
                                attrs: {
                                  to: {
                                    name: "editUser",
                                    params: { id: user.id }
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "Edit Profile\n                                    "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("i", { staticClass: "far fa-user" })
                          ],
                          1
                        )
                      ]
                    )
                  ])
                ])
              }),
              0
            )
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "ul",
      {
        staticClass: "m-0 float-left",
        staticStyle: { "list-style": "none", margin: "0", padding: "0" }
      },
      [
        _c("li", [
          _c("i", { staticClass: "fab fa-facebook-square" }),
          _vm._v(" Facebook")
        ]),
        _vm._v(" "),
        _c("li", [_c("i", { staticClass: "fab fa-twitter-square" })])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/pages/user/users.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/pages/user/users.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _users_vue_vue_type_template_id_47c62e8c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./users.vue?vue&type=template&id=47c62e8c& */ "./resources/js/components/pages/user/users.vue?vue&type=template&id=47c62e8c&");
/* harmony import */ var _users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users.vue?vue&type=script&lang=js& */ "./resources/js/components/pages/user/users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _users_vue_vue_type_template_id_47c62e8c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _users_vue_vue_type_template_id_47c62e8c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/pages/user/users.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/pages/user/users.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/pages/user/users.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./users.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/user/users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/pages/user/users.vue?vue&type=template&id=47c62e8c&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/pages/user/users.vue?vue&type=template&id=47c62e8c& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_users_vue_vue_type_template_id_47c62e8c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./users.vue?vue&type=template&id=47c62e8c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/pages/user/users.vue?vue&type=template&id=47c62e8c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_users_vue_vue_type_template_id_47c62e8c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_users_vue_vue_type_template_id_47c62e8c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);